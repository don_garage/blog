# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
Blog::Application.config.secret_key_base = 'b3d44e34c587c8ac82291aad43e922b98cff0b4e170b5240f6badc13f432e0e6175c2c39a3cc31e280d43d9586d361b1ddb40321937844db6bfc7d05b7d41b65'
